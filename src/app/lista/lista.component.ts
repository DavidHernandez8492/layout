import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent  {

  @Input() Home: string;
  @Input() Services: string;
  @Input() About: string;
  @Input() Contact: string;
  @Input() Faq: string;

  home(){
    alert("Esto nos debe de mostrar el home");
  }
  Service(){
    alert("Esto nos debe de mostrar los servicios");
  }

  Abouts(){
    alert("Esto nos debe mostrar la información acerca de....");
  }
  Contacts(){
    alert("Esto nos debe mostrar la información de contacto");
  }

  Faqs(){
    alert("Esto nos debe mostrar la información");
  }

  constructor() { }

}
