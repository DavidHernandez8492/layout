import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-seccion',
  templateUrl: './seccion.component.html',
  styleUrls: ['./seccion.component.css']
})
export class SeccionComponent {

  public Saludo:string;

  @Input() titulo: string;
  @Input() descripcion: string;

  @Output() Saludame = new EventEmitter();

  constructor() { 
    this.Saludo = "HOLA ESTO ES UN OUTPUT";
  }

  lanzar(event){
    this.Saludame.emit({Saludo : this.Saludo});
  }

  /*Saludo(){
    alert("Estamos aprendiendo a utilizar angular");
  }*/

}
